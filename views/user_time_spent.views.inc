<?php


/**
 *Implements hook_views_data
 */
function user_time_spent_views_data() {
	$table = array();
	$table['time_spent'] = array(
		'table' => array(
			'group' => 'time_spent',
				'base' => array(
					 'field' => 'sno',
					 'title' => 'Time Spent',
					 'help' => 'Time Spent Table'
				),
		),
		'sno' => array(  
			 'title' => t('Serial No'),
			 'help' => t('Time Spent Serial No Field'),
			 'field' => array(
					'click sortable' => TRUE,  
				),
			 'filter' => array(
					 'handler' => 'views_handler_filter_numeric',   
				),  

			 'sort' => array(
					'handler' => 'views_handler_sort',       
			 ),
		 ),
		'uid' => array(
				'title' => t('User Id'),
				'help' => t('Time Spent User Id Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_numeric',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'session_url' => array(
				'title' => t('Session URL'),
				'help' => t('Time Spent Session Url Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_string',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'count' => array(
				'title' => t('Total Count'),
				'help' => ('Time Spent Total Count Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_numeric',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'time' => array(
				'title' => t('Total Time'),
				'help' => t('Time Spent Total Time Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_float',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'created' => array(
				'title' => t('Created'),
				'help' => t('Time Spent Created Field'),
				'field' => array(
            'handler' => 'views_handler_field_date',
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_date',
				),
				'sort' => array(
						'handler' => 'views_handler_sort_date',
				),
		),
		'updated' => array(
				'title' => t('Updated'),
				'help' => t('Time Spent Updated Field'),
				'field' => array(
            'handler' => 'views_handler_field_date',
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_date',
				),
				'sort' => array(
						'handler' => 'views_handler_sort_date',
				),
		),
	);
	$table['time_spent_day'] = array(
		'table' => array(
			'group' => 'time_spent_day',
				'base' => array(
					 'field' => 'sno',
					 'title' => 'Time Spent Day',
					 'help' => 'Time Spent Day Table'
				),
		),
		'sno' => array(  
			 'title' => t('Serial No'),
			 'help' => t('Time Spent Day Serial No Field'),
			 'field' => array(
					'click sortable' => TRUE,  
				),
			 'filter' => array(
					 'handler' => 'views_handler_filter_numeric',   
				),  

			 'sort' => array(
					'handler' => 'views_handler_sort',       
			 ),
		 ),
		'uid' => array(
				'title' => t('User Id'),
				'help' => t('Time Spent Day User Id Field'),
				'relationship' => array(
						'title' => t('User ID'),
						'help' => t('Relate content to the user who created it.'),
						'handler' => 'views_handler_relationship',
						'base' => 'users',
						'base field' => 'uid',
						'label' => t('User ID'),
				),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_numeric',
				),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'session_url' => array(
				'title' => t('Session URL'),
				'help' => t('Time Spent Day Session Url Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_string',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'count' => array(
				'title' => t('Total Count'),
				'help' => ('Time Spent Day Total Count Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_numeric',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'time' => array(
				'title' => t('Total Time'),
				'help' => t('Time Spent Day Total Time Field'),
				'field' => array(
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_float',
				),
				'sort' => array(
						'handler' => 'views_handler_sort',
				),
		),
		'created' => array(
				'title' => t('Created'),
				'help' => t('Time Spent Day Created Field'),
				'field' => array(
            'handler' => 'views_handler_field_date',
						'click sortable' => TRUE,
				),
				'filter' => array(
						'handler' => 'views_handler_filter_date',
				),
				'sort' => array(
						'handler' => 'views_handler_sort_date',
				),
		),
	);
	return $table;
}

/**
 * Implements hook_views_default_views().
 */
function user_time_spent_views_default_views() {
  $views = array();

  $path = drupal_get_path('module', 'user_time_spent') . '/views/includes';
  $view_files = file_scan_directory($path, '/.*\.view\.inc/');
  foreach ($view_files as $file) {
    include DRUPAL_ROOT . '/' . $path . '/' . $file->filename;
    $views[$view->name] = $view;
  }

  return $views;
}
