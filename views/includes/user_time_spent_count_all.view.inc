<?php

$view = new view();
$view->name = 'timespent';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'time_spent';
$view->human_name = 'Time Spent and Count';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE;

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 1;

/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No Result Found';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';


/* Field: Time Spent: Serial No */
/*$handler->display->display_options['fields']['sno']['id'] = 'sno';
$handler->display->display_options['fields']['sno']['table'] = 'time_spent';
$handler->display->display_options['fields']['sno']['field'] = 'sno';
$handler->display->display_options['fields']['sno']['label'] = 'Serial No';*/
/* Field: Time Spent: User id */
$handler->display->display_options['fields']['uid']['id'] = 'uid';
$handler->display->display_options['fields']['uid']['table'] = 'time_spent';
$handler->display->display_options['fields']['uid']['field'] = 'uid';
$handler->display->display_options['fields']['uid']['label'] = 'User ID';
/* Field: Time Spent: Session url */
$handler->display->display_options['fields']['session_url']['id'] = 'session_url';
$handler->display->display_options['fields']['session_url']['table'] = 'time_spent';
$handler->display->display_options['fields']['session_url']['field'] = 'session_url';
$handler->display->display_options['fields']['session_url']['label'] = 'Session URL';
/* Field:Time Spent: Count */
$handler->display->display_options['fields']['count']['id'] = 'count';
$handler->display->display_options['fields']['count']['table'] = 'time_spent';
$handler->display->display_options['fields']['count']['field'] = 'count';
$handler->display->display_options['fields']['count']['label'] = 'Count';
/* Field: Time Spent: Time */
$handler->display->display_options['fields']['time']['id'] = 'time';
$handler->display->display_options['fields']['time']['table'] = 'time_spent';
$handler->display->display_options['fields']['time']['field'] = 'time';
$handler->display->display_options['fields']['time']['label'] = 'Time (Hours)';
/* Field: Time Spent: Created */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'time_spent';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'custom';
$handler->display->display_options['fields']['created']['custom_date_format'] = 'd-m-Y';
$handler->display->display_options['fields']['created']['query_format'] = 1;
$handler->display->display_options['fields']['created']['label'] = 'Created';
/* Field:Time Spent: Updated */
$handler->display->display_options['fields']['updated']['id'] = 'updated';
$handler->display->display_options['fields']['updated']['table'] = 'time_spent';
$handler->display->display_options['fields']['updated']['field'] = 'updated';
$handler->display->display_options['fields']['updated']['date_format'] = 'custom';
$handler->display->display_options['fields']['updated']['custom_date_format'] = 'd-m-Y';
$handler->display->display_options['fields']['updated']['query_format'] = 1;
$handler->display->display_options['fields']['updated']['label'] = 'Updated';

/* Sort:Time Spent: Created */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'time_spent';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'time-spent/all/user';
$handler->display->display_options['menu']['type'] = 'none';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'time spent';
$handler->display->display_options['tab_options']['title'] = 'Time Spent';
$handler->display->display_options['tab_options']['weight'] = '0';




