<?php

$view = new view();
$view->name = 'time_spent_day';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'time_spent_day';
$view->human_name = 'Time Spent and Count Day';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE;

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 1;

/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No Result Found';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';

/* Field: Time Spent Day: Serial No */
/*$handler->display->display_options['fields']['sno']['id'] = 'sno';
$handler->display->display_options['fields']['sno']['table'] = 'time_spent_day';
$handler->display->display_options['fields']['sno']['field'] = 'sno';
$handler->display->display_options['fields']['sno']['label'] = 'Serial No';*/
/* Field: Time Spent Day: User id */
$handler->display->display_options['fields']['uid']['id'] = 'uid';
$handler->display->display_options['fields']['uid']['table'] = 'time_spent_day';
$handler->display->display_options['fields']['uid']['field'] = 'uid';
$handler->display->display_options['fields']['uid']['label'] = 'User ID';
/* Field: Time Spent Day: Session url */
$handler->display->display_options['fields']['session_url']['id'] = 'session_url';
$handler->display->display_options['fields']['session_url']['table'] = 'time_spent_day';
$handler->display->display_options['fields']['session_url']['field'] = 'session_url';
$handler->display->display_options['fields']['session_url']['label'] = 'Session URL';
/* Field: Time Spent Day: Count */
$handler->display->display_options['fields']['count']['id'] = 'count';
$handler->display->display_options['fields']['count']['table'] = 'time_spent_day';
$handler->display->display_options['fields']['count']['field'] = 'count';
$handler->display->display_options['fields']['count']['label'] = 'Count';
/* Field: Time Spent Day: Time */
$handler->display->display_options['fields']['time']['id'] = 'time';
$handler->display->display_options['fields']['time']['table'] = 'time_spent_day';
$handler->display->display_options['fields']['time']['field'] = 'time';
$handler->display->display_options['fields']['time']['label'] = 'Time (Hours)';
/* Field: Time Spent Day: Created */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'time_spent_day';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'custom';
$handler->display->display_options['fields']['created']['custom_date_format'] = 'Y-m-d';
$handler->display->display_options['fields']['created']['label'] = 'Created';



/* Sort: Time Spent Day: Created */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'time_spent_day';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';

 /* Contextual filter: Time Spent Day: User id */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'time_spent_day';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'raw';
$handler->display->display_options['arguments']['uid']['default_argument_options']['index'] = '1';
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '30';
$handler->display->display_options['arguments']['uid']['limit'] = '0';


/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'time-spent/%/user';
$handler->display->display_options['menu']['type'] = 'none';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'time spent';
$handler->display->display_options['tab_options']['title'] = 'Time Spent and Count Day';
$handler->display->display_options['tab_options']['weight'] = '0';




