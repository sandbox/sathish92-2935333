(function($) {
  Drupal.behaviors.time_spent = {
    attach: function(context) {
      var obj = {};
      obj.start = new Date();
      // window.onunload =
      window.onbeforeunload =(function() {
        obj.end = new Date();
        obj.pathname = window.location.pathname;
        $.ajax({
          'type': "POST",
          'url': Drupal.settings.time_spent.base + "/ajax/time/spent",
          'data': {json: JSON.stringify(obj)},
          'dataType': "JSON",
        });
      });
    }
  };
}(jQuery));