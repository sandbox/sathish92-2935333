(function($) {
  $(document).ready(function() {
    if ($('input:radio[name=time_spent_site_choose_url]:checked').val() != 1) {
      $('.form-item-time-spent-site-url').hide();
    }
    $(".form-item-time-spent-site-choose-url input[name='time_spent_site_choose_url']").click(function() {
      if ($('input:radio[name=time_spent_site_choose_url]:checked').val() == 1) {
        $('.form-item-time-spent-site-url').show();
      }
      else {
        $('.form-item-time-spent-site-url').hide();
      }
    });
  });
}(jQuery));