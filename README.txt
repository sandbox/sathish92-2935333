											GUIDELINES FOR USING User Time Spent with count MODULE 
											------------------------------------------------------
OVERVIEW
---------

This module helps to monitor authenticated users. Whenever user gets login, how much time the user spent for a single page.

CONTENTS OF THIS FILE
---------------------
 
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
-------------

*	User Time Spent with count Module is providing with a configuration for roles-based selection.

*	Administrator can able to track the user time spent on the particular node with his/her count on it. 

*	This will also have a configuration to track only for particular URL or site wide URL.


 * For a full description of the module visit:
   
   https://www.drupal.org/sandbox/sathish92/2935333
   
 * To submit bug reports and feature suggestions, or to track changes visit:
   
   https://www.drupal.org/project/issues/2935333

REQUIREMENTS
------------

This module requires two dependent modules as follows
	
	* Date

	* Views


INSTALLATION
------------

Install the User Time Spent module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


CONFIGURATION
-------------

	1. Install this module.

	2. In this callback(/time-spent/all/user), user can get that how much time the user spent for a single page with count.

	3. For the particular user details, get through this callback(time-spent/{user_id}/user) with date specifiction. 


MAINTAINERS
-----------

 *sathish kumar (sathish92) - https://www.drupal.org/u/sathish92
 *Praveen Neelanathan (Praveen Neelanathan) - https://www.drupal.org/u/praveen-neelanathan

